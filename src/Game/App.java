package Game;//Created by alexk on 7/29/2017.

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {

        BallEngine ballEngine = new BallEngine(700, 700);
        primaryStage.setScene(new Scene(new Group(ballEngine.getCanvas())));
        primaryStage.setTitle("Ball Game");
        primaryStage.show();
        ballEngine.startGame();
        primaryStage.setOnCloseRequest(event -> ballEngine.requestStop());
    }

    public static void main(String[] args) {
        System.out.println("main.Start");
        if(args.length > 0) launch(args[0]);
        else launch();
        System.out.println("\nmain.End");
    }
}
