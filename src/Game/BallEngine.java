package Game;//Created by alexk on 7/29/2017.

import Engine.Game;
import Engine.Timer;
import javafx.scene.paint.Color;

import java.util.Random;

public class BallEngine extends Game{
    private final Timer newBallTimer = new Timer();
    private final Random random = new Random();


    public BallEngine(int width, int height){
        super(60, width, height);
    }

    @Override
    protected void init() {
        //System.out.println("Init");
        newBallTimer.set();
    }

    private int nBalls = 0;
    private final int MAXBALLS = 10;
    @Override
    protected void updateWorld() {
        //System.out.println("Update at " + getGameTime());
        if(nBalls < MAXBALLS && newBallTimer.check() > 1.0){
            newBallTimer.set();
            addObj(new BallObj(width/2, height/2, 30, 5,
                    random.nextDouble()*Math.PI*2.0
                    ));
            nBalls++;
        }
    }

    @Override
    protected void drawWorld() {
        //System.out.println("Draw");
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, width, height);
    }

}