package Game;//Created by alexk on 7/29/2017.

import Engine.Obj;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;

public class BallObj implements Obj {
    private int centerX;
    private int centerY;
    private int speedX;
    private int speedY;
    private final int radius;

    public BallObj(int startX, int startY, int radius, int speed, double angle) {
        this.centerX = startX;
        this.centerY = startY;
        this.radius = radius;

        speedX = (int) (Math.cos(angle)*speed);
        speedY = (int) (Math.sin(angle)*speed);

    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setFill(Color.RED);
        gc.fillOval(centerX-radius, centerY-radius, 2*radius, 2*radius);
    }

    @Override
    public void update(int width, int height, List<Obj> objList) {
        centerX+=speedX;
        centerY+=speedY;

        //check wall
        if( (centerX-radius) < 0) speedX=-speedX;
        else if( (centerX+radius) > width) speedX=-speedX;

        if( (centerY-radius) < 0)speedY=-speedY;
        else if( (centerY+radius) > height)speedY=-speedY;

        //check balls
        for (Obj obj : objList){
            if(obj instanceof BallObj && obj!=this){
                BallObj ball =((BallObj) obj);

                double distance = Math.hypot((ball.getCenterX()-this.getCenterX()), (ball.getCenterY()-this.getCenterY()));
                if(distance < radius*2.0){
                    speedY=-speedY;
                    speedX=-speedX;
                }

            }
        }


    }
}
