package Engine;//Created by alexk on 7/29/2017.

public final class Timer {
    private long oldTime = 0;

    public Timer set(){
        oldTime = System.nanoTime();
        return this;
    }

    /**
     *
     * @return the number of seconds since set
     */
    public double check(){
        return (System.nanoTime()-oldTime)/1000000000.0;
    }
}
