package Engine;//Created by alexk on 7/29/2017.

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Game implements Runnable{
    //PRIVATE
    private final Canvas cn;
    private final int MUPS;
    private boolean go = true;
    private Timer gameTimer;
    private List<Obj> objList;
    private void loop(){
        double SPU = 1.0/MUPS;
        Timer timer = new Timer().set();
        while (go){
            update();
            draw();
            while (timer.check() < SPU){
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            timer.set();
        }
    }
    private void update(){
        for(Obj obj : objList){
            obj.update(width, height, objList);
        }
        updateWorld();
    }
    private void draw(){
        drawWorld();
        for(Obj obj : objList){
            obj.draw(gc);
        }
    }
    //PROTECTED
    protected final GraphicsContext gc;
    protected final int width;
    protected final int height;
    public final void requestStop(){
        go = false;
    }
    protected double getGameTime(){
        return gameTimer.check();
    }
    protected abstract void init();
    protected abstract void updateWorld();
    protected abstract void drawWorld();
    protected Game(int MUPS, int width, int height) {
        this.width = width;
        this.height = height;
        cn = new Canvas(width,height);
        gc = cn.getGraphicsContext2D();
        gameTimer = new Timer().set();
        this.MUPS = MUPS;
        objList = new ArrayList<>();
    }

    //PUBLIC
    public final Canvas getCanvas(){
        return cn;
    }
    protected final void addObjs(Obj... objs){
        objList.addAll(Arrays.asList(objs));
    }
    protected final void addObj(Obj obj){
        objList.add(obj);
    }
    @Override
    public final void run() {
        init();
        loop();
    }
    public void startGame(){
        (new Thread(this)).start();
    }
}
