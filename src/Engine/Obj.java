package Engine;//Created by alexk on 7/29/2017.

import javafx.scene.canvas.GraphicsContext;

import java.util.List;

public interface Obj {

     void draw(GraphicsContext gc);

     void update(int gameWidth, int gameHeight, List<Obj> objects);

}
